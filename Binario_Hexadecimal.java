/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LDI_04_Binario_Hexadecimal;
/**
 *
 * @author SONY
 */
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
public class Binario_Hexadecimal extends javax.swing.JFrame{
    long[]binario;
    
    
    public Binario_Hexadecimal() {
        initComponents();
        setLocationRelativeTo(null);
        Image icon=Toolkit.getDefaultToolkit().getImage(getClass().getResource("icono.png"));
        setIconImage(icon);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Conversor Binario-Hexadecimal");
        setResizable(false);

        jButton1.setText("Calcular");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Secuencia de números binarios:");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Secuencia Hexadecimal:");
        jLabel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTextField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField1FocusLost(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1KeyTyped(evt);
            }
        });

        jTextField2.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                    .addComponent(jTextField2))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(jTextField1.getText().matches("[1 0]*")){ //Si se ingresó sólo unos y ceros
            rellenado();
             
            String hexadecimal="";
            
            //Separación en bits de 4:
            long[]cadena_4=new long[4];
            for(int i=0;i<=binario.length-1;){
                for(int ii=0;ii<4;ii++){
                    cadena_4[ii]=binario[i];
                    i++;
                }
                hexadecimal+=busqueda_hexadecimal((int) conversion_decimal(cadena_4));
            }
            jTextField2.setText(hexadecimal);
        }
        else
            JOptionPane.showMessageDialog(this,"Cadena no válida. \n Sólo puede ingresar unos y ceros","Conversor Binario-Hexadecimal",JOptionPane.ERROR_MESSAGE); //Mensaje de error al no ingresar unos y ceros únicamente
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField1FocusGained
        jTextField1.selectAll();
    }//GEN-LAST:event_jTextField1FocusGained

    private void jTextField1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField1FocusLost
        jTextField1.select(0,0);
    }//GEN-LAST:event_jTextField1FocusLost

    private void jTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyTyped
       jTextField2.setText("");
        if(jTextField1.getText().length()==19){
            JOptionPane.showMessageDialog(null,"No puede ingresar más bits.","Conversor Binario-Hexadecimal",JOptionPane.INFORMATION_MESSAGE);
            evt.consume();}        
    }//GEN-LAST:event_jTextField1KeyTyped

    public void rellenado(){
        try{
            long cadena_bin=Long.parseLong(jTextField1.getText()); //Recuperación de la cadena de bits
            int tamaño,faltante;

            tamaño=jTextField1.getText().length(); //Tamaño de la cadena
            faltante=4-(tamaño%4); //Faltante para ser múltiplo de 4
            if(faltante==4)faltante=0; //Si faltan 4 ceros no es necesarios ponerlos (Resulta 4 por la resta cero del residuo menos 4)
            int tamaño_final=tamaño+faltante;

            binario=new long[tamaño_final]; //Nuevo arreglo del tamaño de la cadena + sobrante para guardar los bits y que sea una cantidad múltiplo de cuatro
            tamaño_final--; //Posición del último bit de la cadena
            while(cadena_bin>0){ //LLenado del arreglo con los bits
                binario[tamaño_final--]=cadena_bin%10;
                cadena_bin/=10;} //Termina de llenar con los ceros faltantes a la izquierda para ser múltiplo de 4

            String cadena_nueva="";
            for(int i=0;i<=binario.length-1;i++)
                cadena_nueva+=binario[i];
            jTextField1.setText(cadena_nueva);
        }
        catch(NumberFormatException e){
            System.out.println(e);
        }
    }
    
    public long conversion_decimal(long[]bin){
        int decimal=0,potencia=0;
        for(int x=bin.length-1;x>=0;x--){
            decimal+=(Math.pow(2,potencia))*bin[x];
            potencia++;}
        return decimal;
    }
    
    public String busqueda_hexadecimal(int decimal){
        String hexa="";
        switch(decimal){
            case 0:hexa="0";
                break;
            case 1:hexa="1";
                break;
            case 2:hexa="2";
                break;
            case 3:hexa="3";
                break;
            case 4:hexa="4";
                break;
            case 5:hexa="5";
                break;
            case 6:hexa="6";
                break;
            case 7:hexa="7";
                break;
            case 8:hexa="8";
                break;
            case 9:hexa="9";
                break;
            case 10:hexa="A";
                break;
            case 11:hexa="B";
                break;
            case 12:hexa="C";
                break;
            case 13:hexa="D";
                break;
            case 14:hexa="E";
                break;
            case 15:hexa="F";
                break;
        }
        return hexa;
    }
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Binario_Hexadecimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Binario_Hexadecimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Binario_Hexadecimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Binario_Hexadecimal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Binario_Hexadecimal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
